#!/bin/bash

pause() {
  echo
  echo -n "> Presione la tecla 'c' para continuar o Ctrl+C to exit ... "
  while true; do
    read -n1 -r
    [[ $REPLY == 'c' ]] && break
  done
  clear
  echo "Continuemos ..."
  echo
}

clear

echo "Paso 1) Crea una cuenta en https://launchpad.net, si ya tienes una inicia sesión https://launchpad.net/+login"
pause

echo "Paso 2) Instalaré las librerías y paquetes necesarios, por lo cual es necesario ingreses la contraseña 'sudo' de tu usuario y presionar Enter, luego aguarda un momento por favor..."
sudo add-apt-repository universe
sudo apt update
sudo apt install -y gnupg wget nano haveged rng-tools
clear
echo "Proceso completado!"
pause

echo "Paso 3) Descargando código de conducta"
wget "https://launchpad.net/codeofconduct/2.0/+download" -O $HOME/UbuntuCodeofConduct-2.0.txt
pause

echo "Paso 4) Generando llaves GPG, selecciona la opción 1, cuando te pida el tamaño, selecciona 2048 presionando [Enter] y luego elije la opción sin caducidad escribiendo 0 por último llena tus datos (Nombre, correo, etc) para finalizar presiona V o Y según corresponda en tu caso."
echo
echo "Luego de ingresar tus datos te pedirá una contraseña, para evitar complicaciones te recomendamos dejar este campo vacío"
echo
echo "Si te aparece una VENTANA solicitando una contraseña, puedes colocar allí una clave para asegurar la llave GPG."
echo
echo "Lee las instruccciones anteriores con detalle antes de continuar."
echo
sudo rngd -r /dev/urandom
#gpg --gen-key
gpg --full-generate-key
gpg --list-keys
entropia=$(pidof rngd)
sudo kill -9 $entropia
pause

echo "Paso 5) Estoy enviando tu llave GPG a los servidores de launchpad..."
llave8=$(gpg --fingerprint --keyid-format long | grep -P '\d{4}' | grep pub | grep -o -P '(?<=/)[A-Z0-9]{8}')
llave16=$(gpg --fingerprint --keyid-format long | grep -P '\d{4}' | grep pub | grep -o -P '(?<=/)[A-Z0-9]{16}')
gpg --send-keys --keyserver keyserver.ubuntu.com $llave8
gpg --send-keys --keyserver keyserver.ubuntu.com $llave16
pause

gpg --fingerprint $llave8 | grep =
gpg --fingerprint --keyid-format long $llave16 | grep =

echo
echo "Copia lo que está lineas antes después del símbolo '=' (igual), es algo parecido a esto: AAAA BBBB CCCC DDDD EEEE FFFF GGGG HHHH IIII JJJJ"
echo
echo "Ahora ingresa a: (Cambia NombreDeUsuarioLP por tu nombre de usuario) https://launchpad.net/~NombreDeUsuarioLP/+editpgpkeys y pega el contenido en el campo que allí aparece"

pause

echo "Paso 6) verifica tu correo electrónico, debe haber un mensaje nuevo con el asunto 'Launchpad: Confirm your OpenPGP Key'"
echo
echo "En el correo que te ha llegado encontrarás un mensaje cifrado casi al final del correo"
echo "inicia con:  -----BEGIN PGP MESSAGE-----"
echo "finaliza con:  -----END PGP MESSAGE-----"
echo
echo "============================================="
echo "==========       IMPORTANTE     ============="
echo "============================================="
echo
echo "Es necesario que copies el contenido de la zona que contiene la zona cifrada descrita anteriormente para pegarla a continuación"
echo "Una vez hayas pegado el texto debes presionar las teclas CONTROL + X y luego presiona y o s según corresponda"
nano correo.txt
clear
pause

gpg -d correo.txt | grep https
echo
echo "Paso 7) Ingresa a la URL que aparece anteriormente para validar tu correo electrónico, si no aparece la URL has realizado los pasos de manera incorrecta y te recomendamos iniciar de nuevo"
echo
echo "No continúes hasta que no valides el correo con el link anterior"
pause

echo "Paso 8) Firmar el código de conducta"
echo
echo "Estos son los últimos pasos, de los cuales sugerimos tener especial cuidado y leer las instrucciones con detalle"
gpg --clearsign $HOME/UbuntuCodeofConduct-2.0.txt
echo "A continuación se mostarrá tu código de conducta listo para firmar, debes copiar el contenido que aparece desde el inicio hasta el final"
echo
echo "Debes hacer Scroll con tu puntero para poder leer el contenido."
echo "El código inicia en una sección como esta: '-----BEGIN PGP SIGNED MESSAGE-----'"
echo "El código finaliza en una sección como esta: '-----END PGP SIGNATURE-----' pasando por un texto cifrado"
echo "Deberás copiar todo el texto contenido entre esas dos secciones mencionadas, lo necesitaremos en el paso siguiente"
pause
clear
cat $HOME/UbuntuCodeofConduct-2.0.txt.asc

echo "Todo lo de aqui arriba es lo que debes copiar para pegarlo en el siguiente paso..."
pause

echo "Ingresa a esta URL https://launchpad.net/codeofconduct/2.0/+sign , pega el texto COMPLETO del paso anterior y presiona el botón 'Continue'"
pause

echo "Felicidades, has completado el proceso, para verificar que todo esté en orden, en tu perfil de Launchpad deberás ver una sección con esta información: 'Signed Ubuntu Code of Conduct:Yes '"
echo "¡Gracias por querer ser parte de la familia Ubuntu! =) "
echo
echo "No olvides unirte a nuestro Team para ser miembro oficial de la comunidad: https://launchpad.net/~ubuntu-co "